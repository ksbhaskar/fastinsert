; No claim of copyright is made with respect to this code
; This is an attempt to demonstrate "INSERT" performance by directly setting global
; variable nodes in YottaDB. These nodes can be mapped as a table using Octo for SQL
; access, motivated by https://avi.im/blag/2021/fast-sqlite-inserts/
; - YottaDB: https://yottadb.com & https://gitlab.com/YottaDB/DB/YDB
; - Octo: https://yottadb.com/product/octo-sql-for-analytics/ & https://gitlab.com/YottaDB/DBMS/YDBOcto
; To run this program:
; - Install YottaDB as described at https://yottadb.com/product/get-started/
; - Specify a temporary directory for the test, e.g.: export ydb_dir=/tmp/test
; - Create an environment for the test: source $(pkg-config --variable=prefix yottadb)/ydb_env_set
; - Turn off journaling, as proposed in the performance challenge: mupip set -journal=disable -region "*"
; - Enable the MM access method to maximize performance: mupip set -access_method=mm -region "*"
; - Copy the program file (fastinsert.m) to the $ydb_dir/r directory
; - Run the program: yottadb -run fastinsert <num_nodes> <num_proc>
;   where <num_nodes> is the number of nodes to set (defaulting to 1 million)
;   and <num_proc> is the number of concurrent processes (default to the processor count from /proc/cpuinfo)
; For example on a Raspberry Pi Zero W running Debian Bullseye (32-bits)
;   $ yottadb -run fastinsert 1E6
;   Set 1,000,000 nodes in 75.457866 seconds using 1 processes at 13,252 nodes/second
;   $
; On a Raspberry Pi 3 running Debian Bullseye (64-bits)
;   $ yottadb -run fastinsert 1E7
;   Set 10,000,000 nodes in 46.736418 seconds using 4 processes at 213,966 nodes/second
;   $
; and on a home-built (not overclockded) AMD Ryzen 7 3700X 8-core machine running Ubuntu 21.04 (64-bits):
;   $ yottadb -run fastinsert 1E8
;   Set 100,000,000 nodes in 51.999496 seconds using 16 processes at 1,923,096 nodes/second
;   $
; Note that using YottaDB with journaling disabled, the database is fully persistent on
; normal process completion, but is not recoverable if the system crashes during test.
fastinsert
	set count=+$zpiece($zcmdline," ",1)
	set:'count count=1E6
	set nproc=$zpiece($zcmdline," ",2)
	do:'nproc
	. set io=$io
	. open "/proc/cpuinfo":readonly
	. use "/proc/cpuinfo"
	. for  read line quit:$zeof  if $increment(nproc,line?1"processor".E)
	. use io close "/proc/cpuinfo"
	kill ^ctrl,^user
	set blksiz=count\nproc
	set ^ctrl(1,"first")=1
	for i=2:1:nproc do
	. set ^ctrl(i,"first")=^ctrl(i-1,"first")+blksiz
	. set ^ctrl(i-1,"last")=^ctrl(i,"first")-1
	set ^ctrl(nproc,"last")=count
	set ^ctrl("count")=nproc
	lock +^ctrl("start")
	for i=1:1:nproc job setdata(i)
	for  quit:'^ctrl("count")  hang .001
	lock -^ctrl("start")
	lock +^ctrl("end")
	set end=0,start=$zut
	for i=1:1:nproc do
	. set:start>^ctrl(i,"start") start=^ctrl(i,"start")
	. set:end<^ctrl(i,"end") end=^ctrl(i,"end")
	write "Set ",$fnumber(count,",")," nodes in ",$fnumber(end-start/1E6,",",6)," seconds using ",nproc," processes at ",$fnumber(count/(end-start)*1E6,",",0)," nodes/second",!
	quit

setdata(index)
	new end,first,last,i,start
	lock +^ctrl("end",$job)
	if $increment(^ctrl("count"),-1)
	lock +^ctrl("start",$job)
	set first=^ctrl(index,"first")
	set last=^ctrl(index,"last")
	set start=$zut
	set ^user(first)=1+$random(1E6)_","_(1+$random(3)*5)_","_$random(1)
	for i=first+1:1:last set ^(i)=1+$random(1E6)_","_(1+$random(3)*5)_","_$random(1)
	set end=$zut
	set ^ctrl(index,"start")=start
	set ^ctrl(index,"end")=end
	lock -^ctrl("end",$job)
	quit
